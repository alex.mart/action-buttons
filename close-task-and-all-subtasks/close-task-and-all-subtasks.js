const api= context.getService("fibery");

await Promise.all(args.currentEntities.map(async (e) => {
    const task = await api.getEntityById(e.type, e.id, ["Subtasks"]);
    return Promise.all([
        api.setStateToFinal("Task", task.id),
        ...task["Subtasks"].map(st => api.setStateToFinal("Subtask", st.id))
    ])
}));