const api = context.getService("fibery");

for (const entity of args.currentEntities) {
    const duration = (new Date() - new Date(entity["Timer Started"]))/(60*1000);
    const timeSpent = Number.parseFloat(entity["Time Spent"]) || 0.0;
    await api.updateEntity(entity.type, entity.id, {
        "Timer Started": null,
        "Latest Time Entry": duration,
        "Time Spent": timeSpent + duration
    });
}